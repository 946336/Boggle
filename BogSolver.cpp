// In this issue of "Look at all of these type errors,"
// we see lots and lots of pain

#include <iostream>
#include <fstream>
// Wheeeeeeeeeeeeeeeeeeeeeeeeee
#include <pthread.h>
#include "BogSolver.h"
#include "BogWordList.h"
#include <vector>
#include <cstdio>

using namespace std;

struct Coords{
	Coords(int a, int c, BogSolver *p, vector<BogWordList> *i){
		row = a, col = c, b = p, l = i;
	}

	int row, col;
	BogSolver *b;
	vector<BogWordList> *l;
};

const	char	EDGE = '#';

BogSolver::BogSolver(){
	rows = 0, cols = 0, longestWord = 0;
	board = NULL;
}

// As an STL vector, words will manage its own memory
BogSolver::~BogSolver(){
	for(int i = 0; i < rows; ++i) delete [] board[i];
	delete [] board;
}

// reads the dictionary from cin
bool BogSolver::readDict(){
	string word;
	while(cin >> word){
		if(word != "."){
			up(word);
			dict.insert(word);
			if((int) word.size() > longestWord) longestWord = word.size();
		}
		// correct termination
		else{
			// cout << word << endl;
			return true;
		}
	}
	// something went wrong!
	return false;
}

// reads the boggle board from cin
bool BogSolver::readBoard(){
	char let;

	cin >> rows >> cols;

	board = new char*[rows + 2];
	for(int i = 0; i < rows + 2; ++i){
		board[i] = new char[cols + 2];
	}

	// fill interior with board
	for(int row = 1; row < rows + 1; ++row){
		for(int col = 1; col < cols + 1; ++col){
			if(!(cin >> let)) return false;
			let = toupper(let);
			board[row][col] = let;
		}
	}

	// fill in edges with EDGE character
	for(int row = 0; row < rows + 2; ++row)
		board[row][0] = board[row][cols + 1] = EDGE;
	for(int col = 0; col < cols + 2; ++col)
		board[0][col] = board[rows + 1][col] = EDGE;
	return true;
}

void BogSolver::printBoard(){
	for(int i = 0; i < rows + 2; ++i){
		for(int j = 0; j < cols + 2; ++j){
			cout << board[i][j];
		}
		cout << endl;
	}
}

inline void *BogSolver::makeList(void *b){
	Coords *a = static_cast<Coords*>(b);
	a->l->push_back(*(a->b->findWord(a->row, a->col)));
	pthread_exit(NULL);
	return NULL;
}

// Now, could I get away with spwaning 4 threads at a time here?
// Would data racing be a problem with concurrent addition to the same
// vector?		YES
// Alternatively, have more layers of vectors of vectors, so that each
// thread has its own vector to fill in, and then place those vectors in
// the master vector (yo dawg...?)
	// This way at least we'll be guaranteed shorter vectors, I guess...?
// Uhhhhhhhhhhhh
// lag?
// 	#scarra
bool BogSolver::solve(){
	// for each possible starting position on the board

	int *threads = new int[rows*cols];
	pthread_t *thuds = new pthread_t[rows*cols];

	for(int i = 1; i < rows + 1; ++i){
		for(int j = 1; j < cols + 1; ++j){
			cout << "Solving position (" << i << ", " << j << ")\n";
			// stick the list of words on
			// also good lord this call

			Coords lett(i, j, this, &words);
			cout << "Creating thread " << (j-1)+(i-1)*(j-1) << endl;

			threads[i + i*j] = pthread_create(&(thuds[i + i*j]), NULL,
				&makeList, &lett);

			cout << "Created thread " << (j-1)+(i-1)*(j-1) << endl;

			// words.push_back(*findWord(i, j));

			// if it's empty, then pop it off
			// if(words.back().empty()) words.pop_back();
		}
	}

	cout << "All threads created." << endl << flush;

	// make sure all threads finish
	for(int i = 0; i < rows*cols; ++i){
		if(!pthread_join(thuds[i], NULL)){
			printf("Failed to join thread %d\n", i);
		}
		else {
			printf("Waiting for thread %d\n", i);
		}
	}

	printf("Threads have finished.\n");

	compileFinalList();
	// need to fiure out failure condition...?
	return true;
}

// AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
// mash all the separate lists together
void BogSolver::compileFinalList(){
	// preallocate the final solution vector
	int totalSize = 0;
	for(int i = 0; i < rows*cols; ++i){
		totalSize += words[i].size();
	}
	finalList.resize(totalSize);

	int it = 0;
	for(int i = 0; i < (int) words.size(); ++i){
		for(int j = 0; j < (int) words[i].size(); ++j){
			finalList[it++] = (words[i][j]);
			// finalList.push_back(words[i][j]);
		}
	}
}

BogWordList* BogSolver::findWord(BogLett b){
	BogWordList *currList = new BogWordList;
	BogWord *sequence = new BogWord;

	int row = b.row;
	int col = b.col;

	// // grab the thingama-node-r
	// Node *dictRoot = dict.getRoot();
	// Node *spot = dictRoot->findChild(board[row][col]);

	// start @ correct letter with empty BogWords, empty list, and 0 depth
	// currList is modified through reference access, which makes life
		// a little easier
	findWord(row, col, 0, sequence, currList);
	// give back the damn list
	return currList;
}

// spawn the recursive thingamadoohicker
// i.e. wrap for root of recursive call
BogWordList* BogSolver::findWord(int row, int col){
	BogWordList *currList = new BogWordList;
	BogWord *sequence = new BogWord;

	// // grab the thingama-node-r
	// Node *dictRoot = dict.getRoot();
	// Node *spot = dictRoot->findChild(board[row][col]);

	// start @ correct letter with empty BogWords, empty list, and 0 depth
	// currList is modified through reference access, which makes life
		// a little easier
	findWord(row, col, 0, sequence, currList);
	// give back the damn list
	return currList;
}

// recursive check for words, recursion depth should never exceed length
// of longest word + 1 or so
bool BogSolver::findWord(int row, int col, int depth, BogWord *sequence,
						 BogWordList *currList){
	// if current letter is EDGE, end current chain
	// if depth is equal to longestWord, end current chain
	// check if adding current letter to past sequence follows trie path
	// if not, end current chain
	// check if spot has been visited before (in sequence!)
	// add current letter to sequence
	// check for end of word
		// if yes, add word to the list of words
	// check the 8 adjacent spots (clockwise from 12 o'clock) with
		// depth + 1

	printf("(%d, %d), Depth: %d Letter: %c", row, col, depth, board[row][col]);
	cout << " Current sequence: " << wordToStr(sequence) << endl;

	if(board[row][col] == EDGE){
		cout << "Termination: edge of board\n";
		return false;
	}
	if(depth == longestWord){
		cout << "Termination: longer than longest word\n";
		return false;
	}
	if(visited(row, col, sequence)){
		cout << "Termination: spot visited\n";
		return false;
	}

	// log this spot
	sequence->push_back(BogLett(board[row][col], row, col));

	if(!dict.isPrefix(wordToStr(sequence))){
		cout << "Termination: not a word. Killing character\n";
		sequence->pop_back();
		return false;
	}

	// for(size_t i = 0; i < sequence->size(); ++i){
	// 	cout << sequence->at(i).c;
	// }
	// if(depth != 0) printf("Current dictionary spot: %c", dictSpot->content());
	// cout <<endl;

	// cout << std::flush;
	// cout << "Places.\n";

	// log sequence if it's a word
	if(dict.isWord(wordToStr(sequence))){
		cout << "Found: ";
		for(size_t i = 0; i < sequence->size(); ++i){
			cout << sequence->at(i).c;
			if(sequence->at(i).c == 'Q') cout << 'U';
		}
		currList->push_back(*sequence);
		cout << endl;
	}
	else {
		if(!dict.isPrefix(wordToStr(sequence))){
			cout << "Not a prefix.\n";
			// cout << "Not a prefix. Killing chain.\n" << flush;
			// return false;
		}
		else {
			cout << "Is a prefix.\n";
		}
	}

	// cout << " Or places.\n";
	// cout << std::flush;

	// continue to next layer/character
	++depth;
	if(!checkAround(row, col, depth, sequence, currList)){
		cout << "No words found down this character! Popping character!\n";
		// sequence->pop_back();
		cout << "Sequence is: " << wordToStr(sequence) << " from "
			 << "(" << row << ", " << col << ")" << endl;
	}
	return true;
}

bool BogSolver::visited(int row, int col, BogWord *sequence){
	for(BogWord::iterator it = sequence->begin(); it != sequence->end();
					++it){
		if(it->row == row and it->col == col) return true;
	}
	return false;
}

// look at all of these nasty function calls
// I'm actually worried that this might slow the program down,
	// even though it's inline
inline bool BogSolver::checkAround(int row, int col, int depth,
							BogWord *sequence, BogWordList* currList){
	bool none = true;
	cout << "Current depth: " << depth << endl;
	// skip the 'U' if we're on a 'Q'
	// if(sequence->at(depth - 1).c == 'Q'){
	// 	++depth;
	// }
	// clockwise from 12 o'clock
	if(findWord(row - 1, col, depth, sequence, currList))
		none = false;

	if(findWord(row - 1, col + 1, depth, sequence, currList))
		none = false;

	if(findWord(row, col + 1, depth, sequence, currList))
		none = false;

	if(findWord(row + 1, col + 1, depth, sequence, currList))
		none = false;

	if(findWord(row + 1, col, depth, sequence, currList))
		none = false;

	if(findWord(row + 1, col - 1, depth, sequence, currList))
		none = false;

	if(findWord(row, col - 1, depth, sequence, currList))
		none = false;

	cout << "This is the final check for spot (" << row << ", " << col << ")\n";
	if(findWord(row - 1, col - 1, depth, sequence, currList)){
		none = false;
	}
	sequence->pop_back();
	return !none;
}

int BogSolver::numWords(){
	int totalWords = 0;
	for(int i = 0; i < (int) words.size(); ++i){
		totalWords += words[i].size();
	}
	return totalWords;
}

// words is of type vector<BogWordList*>
// words[i] is of type BogWordList
// words [i][j] is of type BogWord
// brain is of type exploded
int BogSolver::numWords(int len){
	int numLen = 0;
	for(int i = 0; i < (int) words.size(); ++i){
		for(int j = 0; j < (int) words[i].size(); ++j){
			if((int) words[i][j].size() == len) ++numLen;
		}
	}
	return numLen;
}

// godammit. Why you gotta make things hard?
BogWordList* BogSolver::getWords(){
	// // I believe vectors have the big three
	// BogWordList* newList = new BogWordList(finalList);
	// return newList;
	return &finalList;
}

BogWordList* BogSolver::getWords(int len){
	BogWordList* listLen = new BogWordList;
	for(int i = 0; i < (int) finalList.size(); ++i){
		if((int) finalList[i].size() == len){
			listLen->push_back(finalList[i]);
		}
	}
	return listLen;
}

void BogSolver::up(string &s){
	for(size_t i = 0; i < s.length(); ++i){
		s[i] = toupper(s[i]);
	}
}

// AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

// print Qu instead of just q
void BogSolver::printWords(){
	// cout << finalList.size() << endl;

	for(BogWordList::iterator lt = finalList.begin(); lt != finalList.end();
			++lt){
		cout << "<";
		for(BogWord::iterator wd = lt->begin(); wd != lt->end(); ++wd){
			if(wd->c == 'Q')
				cout << " " << wd->c << "u " << wd->row << " " << wd->col;
			else
				cout << " " << wd->c << " " << wd->row << " " << wd->col;
		}
		cout << " " << ">" << endl;
	}
}

void BogSolver::printWords(int len){
	for(BogWordList::iterator lt = finalList.begin(); lt != finalList.end();
			++lt){
		if(length(lt) == len){
			cout << "<";
			for(BogWord::iterator wd = lt->begin(); wd != lt->end(); ++wd){
				if(wd->c == 'Q')
				cout << " " << wd->c << "u " << wd->row << " " << wd->col;
			else
				cout << " " << wd->c << " " << wd->row << " " << wd->col;
			}
			cout << " " << ">" << endl;
		}
	}
}

void BogSolver::listWords(){
	cout << finalList.size() << endl;
	for(BogWordList::iterator lt = finalList.begin(); lt != finalList.end();
			++lt){
		for(BogWord::iterator wd = lt->begin(); wd != lt->end(); ++wd){
			if(wd->c == 'Q')
				cout << wd->c << 'U';
			else
				cout << wd->c;
		}
		cout << endl;
	}
}

void BogSolver::listWords(int len){
	for(BogWordList::iterator lt = finalList.begin(); lt != finalList.end();
			++lt){
		if(length(lt) == len){
			for(BogWord::iterator wd = lt->begin(); wd != lt->end(); ++wd){
				if(wd->c == 'Q')
					cout << wd->c << 'U';
				else
					cout << wd->c;
			}
			cout << endl;
		}
	}
}

// resolve true length of word
int BogSolver::length(BogWordList::iterator it){
	int wordLength = 0;
	for(BogWord::iterator i = it->begin(); i != it->end(); ++i){
		++wordLength;
		if(i->c == 'Q') ++wordLength;
	}
	return wordLength;
}
