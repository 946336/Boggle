#ifndef BOGSOLVER_H
#define BOGSOLVER_H

#include "BogWordList.h"
#include "Dictionary.h"

//
// A class to solve boggle puzzles
//  The class reads in a dictionary and a board
//  Then solves it.
//  Once it has solved it, the solver can be
//  asked for the words it found, can be asked to print the words
//  with their locations, and to print the words just as strings
//

class BogSolver
{
    public:
	BogSolver();
	~BogSolver();
	bool readDict();
	bool readBoard();
	bool solve();			// search board for words in dict
	int  numWords();		// returns number of words found
	int  numWords(int len);		// number of words of length len
	BogWordList* getWords();	// returns all words found
	BogWordList* getWords(int len);	// returns words of len len
	void printWords();		// print all words in HBF
	void printWords(int len);	// print len-length words in HBF
	void listWords();		// print just the text, no coords
	void listWords(int len);	// just the text, no coords

	void printBoard();

   private:
	Dictionary dict;		// must use a Dictionary
	int rows, cols, longestWord;
	char** board;

	// What the hell, brain
	vector<BogWordList> words;
	BogWordList finalList;
	void compileFinalList();

	BogWordList* findWord(int row, int col);
	BogWordList* findWord(BogLett b);
	bool findWord(int row, int col, int depth, BogWord *sequence,
				  BogWordList *currList);
	// look at all of this unnecessary parceling
	inline bool checkAround(int row, int col, int depth, BogWord *sequence,
						   BogWordList *currList);
	// modifies object instead of returning it!
	void up(std::string &s);
	// expectes the iterator to already point to the word you want
	// checked!
	int length(BogWordList::iterator it);
	bool visited(int row, int col, BogWord *sequence);
	static inline void *makeList(void* b);
};
#endif
