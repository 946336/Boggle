#ifndef BOGSCORER_H
#define BOGSCORER_H

#include "BogWordList.h"
#include <vector>
#include <string>

class BogScorer{
public:
	BogScorer();
	~BogScorer();

	void readAnswers();
	void scoreAnswers();
	void printAnswers();
private:
	std::vector<std::string> lines, results;
	int *scoreLookup;
	int totalScore, numWords;

	int getScore(int length);
};

#endif