#include "BogScorer.h"
#include <sstream>
#include <cstdio>
#include <sstream>
#include <iostream>

BogScorer::BogScorer(){
	totalScore = 0, numWords = 0;
	scoreLookup = new int[5];
	scoreLookup[0] = 1;
	scoreLookup[1] = 2;
	scoreLookup[2] = 3;
	scoreLookup[3] = 5;
	scoreLookup[4] = 11;
}

BogScorer::~BogScorer(){
	delete [] scoreLookup;
}

void BogScorer::readAnswers(){
	std::string line;
	while(std::getline(std::cin, line)) lines.push_back(line);
}

void BogScorer::scoreAnswers(){
	std::string verdict;
	std::stringstream ss;
	for(std::vector<std::string>::iterator it = lines.begin();
		it != lines.end(); ++it){
		if(it[0] == "N"){
			verdict = "0 " + *it;
		}
		else if(it[0] == "O"){
			// eat the designation and space
			int score = getScore(it->length() - 3);
			totalScore += score;
			++numWords;
			ss << score;
			ss >> verdict;
			verdict = verdict + " " + *it;
		}
		results.push_back(verdict);
	}
}

void BogScorer::printAnswers(){
	for(std::vector<std::string>::iterator it = results.begin();
		it != results.end(); ++it){
		std::cout << *it << std::endl;
	}
}

int BogScorer::getScore(int length){
	switch(length){
		case 0:
		case 1:
		case 2:
			return 0;
		case 3:
		case 4:
			return scoreLookup[0];
		case 5:
			return scoreLookup[1];
		case 6:
			return scoreLookup[2];
		case 7:
			return scoreLookup[3];
		default:
			return scoreLookup[4];
	}
}
