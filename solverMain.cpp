#include "BogWordList.h"
#include "Dictionary.h"
#include "BogSolver.h"

#include <iostream>

using namespace std;

int main()
{
	BogSolver solver;

	solver.readDict();
	solver.readBoard();
	// solver.printBoard();
	solver.solve();
	cout << endl << "Words found:\n" << flush;
	// solver.printWords();
	solver.listWords(5);
	return 77;
}

