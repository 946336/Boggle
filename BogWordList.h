#ifndef BOGWORDLIST_H
#define BOGWORDLIST_H

//
// A solution to boggle is a list of words.
// Each word is a sequence of letters at specific locations
// 	The locations are given as row and column,
// 	where the upper left is (0,0)
//
// a BogWordList is a struct holding a count of words
//   and a pointer to an array of words.  
//
// Each word is a struct holding a count of letters 
//   and a pointer to an array of letters
//
// Each letter is struct holding a char, the row, the col
//

#include <vector>
#include <string>

struct BogLett {
	BogLett(){c = '#', row = -1, col = -1;}
	BogLett(char a, int r, int d){c = a, row = r, col = d;}

	char	c;
	int	row, col;
};
typedef std::vector<BogLett> BogWord;
typedef std::vector<BogWord> BogWordList;

static __attribute__((unused)) std::string wordToStr(BogWord *b){
	std::string str;

	if(b->empty()) return std::string();

	for(BogWord::iterator it = b->begin(); it != b->end();
		++it){
		str += it->c;
		if(it->c == 'Q') str += 'U';
	}
	return str;
}

#endif
