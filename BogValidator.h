#ifndef BOGVALIDATOR_H
#define BOGVALIDATOR_H

#include "Dictionary.h"
#include "BogWordList.h"

class BogValidator
{
public:
	BogValidator(); // constructor
	~BogValidator(); // destructor
	bool readDict(); // read in a dictionary
	bool readBoard(); // read in a board
	bool isValid(std::string s); // validates one word
	void checkWords(); // validates cin
private:
	int rows, cols;
	BogLett **board;
	Dictionary dict; // must use a Dictionary

	bool findWord(std::string s);
	bool findWord(std::string s, int depth, int row, int col,
				  BogWord *path);
	bool visited(int row, int col, BogWord *path);
	// modifies object instead of returning it!
	void up(std::string &s);
};

#endif

