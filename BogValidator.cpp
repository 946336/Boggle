#include "BogValidator.h"
#include "BogWordList.h"
#include <cctype>

const string	ENDDICT		= ".";
const char		EDGE		= '#';

BogValidator::BogValidator(){
	board = NULL;
}

BogValidator::~BogValidator(){
	for(int i = 0; i < rows + 2; ++i) delete [] board[i];
		delete [] board;
}

bool BogValidator::readDict(){
	string hey;
	while(cin >> hey){
		if(hey == ENDDICT) break;
		else{
			up(hey);
			if(!dict.insert(hey)) return false;
		}
	}
	return true;
}

// reads the boggle board from cin
bool BogValidator::readBoard(){
	char let;

	cin >> rows >> cols;

	board = new BogLett*[rows + 2];
	for(int i = 0; i < rows + 2; ++i){
		board[i] = new BogLett[cols + 2];
	}

	// fill interior with board
	for(int row = 1; row < rows + 1; ++row){
		for(int col = 1; col < cols + 1; ++col){
			if(!(cin >> let)) return false;
			let = toupper(let);
			board[row][col] = BogLett(let, row, col);
		}
	}

	// fill in edges with EDGE character
	// +1 b/c rows/cols are interior dimensions
	for(int row = 0; row < rows + 2; ++row){
		board[row][0] = BogLett(EDGE, row, 0);
		board[row][cols + 1] = BogLett(EDGE, row, cols + 1);
	}
	for(int col = 0; col < cols + 2; ++col){
		board[0][col] = BogLett(EDGE, 0, col);
		board[rows + 1][col] = BogLett(EDGE, rows + 1, col);
	}
	return true;
}

bool BogValidator::isValid(string s){
	if(!dict.isWord(s)) return false;
	return findWord(s);
}

// find the place where the word starts
bool BogValidator::findWord(string s){
	BogWord path;
	bool found = false;
	for(int i = 1; i <= rows; ++i){
		for(int j = 1; j <= cols; ++j){
			if(board[i][j].c == s[0]){
				// depth of 1 since first letter has been found, unless
				// first letter is 'Q'
				if(board[i][j].c == 'Q')
					found = findWord(s, 2, i, j, &path);
				else
					found = findWord(s, 1, i, j, &path);
				// escape loops awkwardly
				if(found) i = rows, j = cols;
			}
		}
	}
	return found;
}

// needs path tracking capabilities
bool BogValidator::findWord(string s, int depth, int row, int col,
							BogWord *path){
	if(visited(row, col, path)) return false;

	if(board[row][col].c == s[depth]){
		path->push_back(board[row][col]);
	}
	else return false;

	// skip looking for the 'u,' since it's assumed and not on the board
	if(board[row][col].c == 'Q') ++depth;

	int rowNext[8] = {-1, -1, 0, 1, 1, 1, 0, -1};
	int colNext[8] = {0, 1, 1, 1, 0, -1, -1, -1};

	for(int i = 0; i < 8; ++i){
		if(findWord(s, depth + 1, row + rowNext[i],
			col + colNext[i], path)){
			return true;
		}
	}
	return false;
}

// Disallow re-visits of any spot on this run
bool BogValidator::visited(int row, int col, BogWord *path){
	for(BogWord::iterator it = path->begin();
		it != path->end(); ++it){
		if(row == it->row and col == it->col) return false;
	}
	return true;
}

// does shit with cin
void BogValidator::checkWords(){
	string word;
	while(cin >> word){
		up(word);
		if(isValid(word)){
			cout << "OK " << word << endl;
		}
		else {
			cout << "NO " << word << endl;
		}
	}
}

// modifies object instead of returning it!
void BogValidator::up(string &s){
	for(size_t i = 0; i < s.length(); ++i){
		s[i] = toupper(s[i]);
	}
}
